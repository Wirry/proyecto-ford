'use strict'

var mongoose = require ('mongoose');
var Schema = mongoose.Schema;

var AveriaSchema = Schema ({
    fecha: String,
    hora: String,
    seleccion: String,
    orden: String,
    tiempo: String,
    averia: String,
    causa: String,
    accion: String,
    /* imagen: String, */

});

module.exports = mongoose.model ('Averia', AveriaSchema);
// averias --> guarda los documentos en la colección