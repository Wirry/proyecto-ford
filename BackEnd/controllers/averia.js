'use strict'

/* const { resolveProjectReferencePath } = require('typescript'); */

var Averia = require ('../models/averia');
var fs = require ('fs');

/*   const { param } = require('../routes/averia'); */

var controller = {
    home : function (req, res){
        return res.status (200).send({
            message: 'Soy la home'
        });

    },

    test: function (req, res){
        return res.status (200).send ({
            message: 'Soy el metodo o acción test del controlador de averias'
        });
    },

    saveAveria: function (req, res){
        var averia = new Averia();

        var params = req.body;
        averia.fecha = params.fecha;
        averia.hora = params.hora;
        averia.seleccion = params.seleccion;
        averia.orden = params.orden;
        averia.tiempo = params.tiempo;
        averia.averia = params.averia;
        averia.causa = params.causa;
        averia.accion = params.accion;
/*         averia.foto = params.foto;*/

        averia.save((err, averiaStored) => {
            if(err) return res.status(500).send ({message: 'Error al guardar el documento.'});

            if (!averiaStored) return res.status(404).send ({message: 'No se ha podido guardar la averia'});

            return res.status (200).send ({averia: averiaStored});
        });
    },    

    getAveria: function (req, res){
        var averiaId = req.params.id;

        if (averiaId == null) return res.status(404).send({message: 'La averia no existe'});

        Averia.findById(averiaId, (err, averia) => {

            if (err) return res.status(500).send ({message: 'Error al devolver los datos.'});

            if (!averia) return res.status(404).send({message: 'La averia no existe'});

            return res.status(200).send({
                    averia
            });

        });
    
    },

    getAverias: function (req, res){

        Averia.find ({}).exec ((err, averias) => {

            if (err) return res.status(500).send({message: 'Error al devolver los datos.'});

            if(!averias) return res.status(404).send ({ message: 'No hay averias que mostrar.'});

            return res.status(200).send({averias});
        });
    },

    updateAveria: function (req, res){
        var averiaId = req.params.id;
        var update = req.body;

        Averia.findByIdAndUpdate (averiaId, update, {new:true}, (err, averiaUpdated) => {
            if (err) return res.status(500).send ({message: 'Error al actualizar'});

            if(!averiaUpdated) return res.status(404).send ({ message: 'No existe la averia para actualizar.'});

            return res.status(200).send({
                averia: averiaUpdated
            });
        }); 

    },

//METODO PARA BORRAR AVERÍA
    deleteAveria: function (req, res){
        var averiaId = req.params.id;

        Averia.findByIdAndRemove(averiaId, (err, averiaRemoved) => {
            if (err) return res.status(500).send ({message: 'No se ha podido borrar la avería.'});

            if (!averiaRemoved) return res.status (404).send ({ message: 'No se puede eliminar esa avería.'});

            return res.status(200).send ({
                averia: averiaRemoved
            });
        });
    },

//METODO PARA SUBIR IMAGENES
    uploadImage: function (req, res) {
        var averiaId = req.params.id;
        var fileName = 'Imagen no subida...';

        if (req.files) {

            var filePath = req.files.image.path;
            var fileSplit = filePath.split('\\');
            var fileName = fileSplit[1];
            var extSplit = fileName.split ('\.');
            var fileExt = extSplit[1];

            if (fileExt == 'png' || fileExt == 'jpg' || fileExt == 'jpeg' || fileExt == 'gif') {

                Averia.findByIdAndUpdate (averiaId, {image: fileName}, {new: true}, (err, averiaUpdated) => {

                    if (err) return res.status (500).send ({mesage: 'La imagen no se ha subido'});
    
                    if (!averiaUpdated) return res.status (404). send ({ message: 'La avería no existe y no se ha guardado la imagen'});
    
                    return res.status (200).send ({
                        averia: averiaUpdated
        
                    });
    
                });

            }else {

                fs.unlink (filePath, (err) => {
                    return res.status (200). send({ message: 'La extensión no es valida'});

                });

            }




        } else {
            return res.status (200).send ({
                message: fileName
            });
        }   
    }

};

module.exports = controller;