'use strict'

var express = require ('express');
var AveriaController = require ('../controllers/averia');

var router = express.Router ();

var multipart = require ('connect-multiparty');
var multipartMiddleware = multipart({ uploadDir: './uploads' });

router.get ('/home', AveriaController.home);
router.post ('/test', AveriaController.test);
router.post ('/save-averia', AveriaController.saveAveria);
router.get ('/averia/:id?', AveriaController.getAveria );
router.get ('/averias', AveriaController.getAverias);
router.put ('/averia/:id', AveriaController.updateAveria);
router.delete ('/averia/:id', AveriaController.deleteAveria);
router.post ('/upload-image/:id', multipartMiddleware, AveriaController.uploadImage);


module.exports = router;