import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-l107',
  templateUrl: './dialog-l107.component.html',
  styleUrls: ['./dialog-l107.component.css']
})
export class DialogL107Component implements OnInit {

  Activo: string;

  Linea: string = 'Línea 107';

  constructor( public dialog: MatDialog ) { }

  ngOnInit(): void {
  }

  R0D(){
    this.Activo = 'R0D';
  }

  Desapilador(){
    this.Activo = 'Desapilador';
  }

  R0C(){
    this.Activo = 'R0C';
  }

  Prensa_1(){
    this.Activo = 'Prensa 1';
  }

  R1(){
    this.Activo = 'R1';
  }

  R2(){
    this.Activo = 'R2';
  }

  Winch_1(){
    this.Activo = 'Winch (1/2)';
  }

  Prensa_2(){
    this.Activo = 'Prensa 2';
  }

  R3(){
    this.Activo = 'R3';
  }

  Winch_2(){
    this.Activo = 'Winch (2/3)';
  }

  Prensa_3(){
    this.Activo = 'Prensa 3';
  }

  R4(){
    this.Activo = 'R4';
  }

  Winch_3(){
    this.Activo = 'Winch (3/4)';
  }

  Prensa_4(){
    this.Activo = 'Prensa 4';
  }

  R5(){
    this.Activo = 'R5';
  }

  Winch_4(){
    this.Activo = 'Winch (4/5)';
  }

  Prensa_5(){
    this.Activo = 'Prensa 5';
  }

  R6(){
    this.Activo = 'R6';
  }

  Winch_5(){
    this.Activo = 'Winch (5)';
  }

  R7(){
    this.Activo = 'R7';
  }

  R8(){
    this.Activo = 'R8';
  }

  Apilado(){
    this.Activo = 'Apilado';
  }

  Banda(){
    this.Activo = 'Banda';
  }

}
