import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DialogL107Component } from './Dialog-L107.component';

describe('DialogL107Component', () => {
  let component: DialogL107Component;
  let fixture: ComponentFixture<DialogL107Component>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogL107Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogL107Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
