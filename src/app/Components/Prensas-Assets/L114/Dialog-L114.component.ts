import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-l114',
  templateUrl: './dialog-l114.component.html',
  styleUrls: ['./dialog-l114.component.css']
})
export class DialogL114Component implements OnInit {


  Activo: string;

  Linea: string = 'Línea 114';

  constructor( public dialog: MatDialog ) { }

  ngOnInit(): void {
  }

  Desapilador(){
    this.Activo = 'Desapilador';
  }

   Prensa(){
    this.Activo = 'Prensa';
  }

  Banda_1(){
    this.Activo = 'Banda 1';
  }

  Banda_2(){
    this.Activo = 'Banda 2';
  }

}
