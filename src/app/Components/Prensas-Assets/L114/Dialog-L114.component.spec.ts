import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DialogL114Component } from './Dialog-L114.component';

describe('DialogL114Component', () => {
  let component: DialogL114Component;
  let fixture: ComponentFixture<DialogL114Component>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogL114Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogL114Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
