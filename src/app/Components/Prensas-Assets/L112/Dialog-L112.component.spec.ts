import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DialogL112Component } from './Dialog-L112.component';

describe('DialogL112Component', () => {
  let component: DialogL112Component;
  let fixture: ComponentFixture<DialogL112Component>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogL112Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogL112Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
