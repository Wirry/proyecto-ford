import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-l112',
  templateUrl: './dialog-l112.component.html',
  styleUrls: ['./dialog-l112.component.css']
})
export class DialogL112Component implements OnInit {

  Activo: string;

  Linea: string = 'Línea 112';

  constructor( public dialog: MatDialog ) { }

  ngOnInit(): void {
  }

  R1(){
    this.Activo = 'R1';
  }

  Lavadora(){
    this.Activo = 'Lavadora';
  }

  Desapilador(){
    this.Activo = 'Desapilador';
  }

  R2(){
    this.Activo = 'R2';
  }

  Prensa_1(){
    this.Activo = 'Prensa 1';
  }

  VD1(){
    this.Activo = 'Volvo Descarga 1';
  }

  Transportador_1(){
    this.Activo = 'Transportador 1';
  }

  VC2(){
    this.Activo = 'Volvo Carga 2';
  }

  Prensa_2(){
    this.Activo = 'Prensa 2';
  }
  VD2(){
    this.Activo = 'Volvo Descarga 2';
  }

  Transportador_2(){
    this.Activo = 'Transportador 2';
  }

  VC3(){
    this.Activo = 'Volvo Carga 3';
  }

  Prensa_3(){
    this.Activo = 'Prensa 3';
  }

  R4(){
    this.Activo = 'R4';
  }

  Prensa_4(){
    this.Activo = 'Prensa 4';
  }

  R5(){
    this.Activo = 'R5';
  }

  Prensa_5(){
    this.Activo = 'Prensa 5';
  }

  R1_ABB(){
    this.Activo = 'R1 ABB';
  }

   Banda1(){
    this.Activo = 'Banda 1';
  }

  Banda2(){
    this.Activo = 'Banda 2';
  }

}
