import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DialogU103Component } from './Dialog-U103.component';

describe('DialogU103Component', () => {
  let component: DialogU103Component;
  let fixture: ComponentFixture<DialogU103Component>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogU103Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogU103Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
