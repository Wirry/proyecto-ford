import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-u103',
  templateUrl: './dialog-u103.component.html',
  styleUrls: ['./dialog-u103.component.css']
})
export class DialogU103Component implements OnInit {

  Activo: string;

  Linea: string = 'Línea U103';

  constructor( public dialog: MatDialog ) { }

  Devanadora(){
    this.Activo = 'Devanadora';
  }

  Enderezadora(){
    this.Activo = 'Enderezadora';
  }

  Prensa(){
    this.Activo = 'Prensa';
  }

  Apilador_lateral(){
    this.Activo = 'Apilador Lateral';
  }

  Apilador_trasero(){
    this.Activo = 'Apilador Trasero';
  }

  Cizalla(){
    this.Activo = 'Cizalla';
  }

  ngOnInit(): void {
  }

}
