import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DialogL108Component } from './Dialog-L108.component';

describe('DialogL108Component', () => {
  let component: DialogL108Component;
  let fixture: ComponentFixture<DialogL108Component>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogL108Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogL108Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
