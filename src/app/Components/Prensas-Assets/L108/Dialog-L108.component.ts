import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-l108',
  templateUrl: './dialog-l108.component.html',
  styleUrls: ['./dialog-l108.component.css']
})
export class DialogL108Component implements OnInit {

  Activo: string;

  Linea: string = 'Línea 108';

  constructor( public dialog: MatDialog ) { }

  ngOnInit(): void {
  }

  AR1(){
    this.Activo = 'AR1';
  }

  Desapilador(){
    this.Activo = 'Desapilador';
  }

  AR0(){
    this.Activo = 'AR0';
  }

  Prensa_1(){
    this.Activo = 'Prensa 1';
  }

  R2U(){
    this.Activo = 'R2U';
  }

  R2L(){
    this.Activo = 'R2L';
  }

  Prensa_2(){
    this.Activo = 'Prensa 2';
  }

  R3(){
    this.Activo = 'R3';
  }

  Prensa_3(){
    this.Activo = 'Prensa 3';
  }

  R4(){
    this.Activo = 'R4';
  }

  Prensa_4(){
    this.Activo = 'Prensa 4';
  }

  R5(){
    this.Activo = 'R5';
  }

  Prensa_5(){
    this.Activo = 'Prensa 5';
  }

  R6(){
    this.Activo = 'R6';
  }

  Banda(){
    this.Activo = 'Banda';
  }

}
