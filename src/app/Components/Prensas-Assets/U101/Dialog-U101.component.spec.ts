import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DialogU101Component } from './Dialog-U101.component';

describe('DialogU101Component', () => {
  let component: DialogU101Component;
  let fixture: ComponentFixture<DialogU101Component>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogU101Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogU101Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
