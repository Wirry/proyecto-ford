import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-u101',
  templateUrl: './dialog-u101.component.html',
  styleUrls: ['./dialog-u101.component.css']
})
export class DialogU101Component implements OnInit {

  Activo: string;

  Linea: string = 'Línea U101';

  constructor( public dialog: MatDialog ) { }

  ngOnInit(): void {
  }

  Devanadora(){
    this.Activo = 'Devanadora';
  }

  Enderezadora(){
    this.Activo = 'Enderezadora';
  }

  Prensa(){
    this.Activo = 'Prensa';
  }

  Apilador_lateral(){
    this.Activo = 'Apilador Lateral';
  }

  Apilador_trasero(){
    this.Activo = 'Apilador Trasero';
  }

  Cizalla(){
    this.Activo = 'Cizalla';
  }

}
