import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DialogL106Component } from './Dialog-L106.component';

describe('DialogL106Component', () => {
  let component: DialogL106Component;
  let fixture: ComponentFixture<DialogL106Component>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogL106Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogL106Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
