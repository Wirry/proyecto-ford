import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-l106',
  templateUrl: './dialog-l106.component.html',
  styleUrls: ['./dialog-l106.component.css']
})
export class DialogL106Component implements OnInit {


  Activo: string;

  Linea: string = 'Línea 106';

  constructor( public dialog: MatDialog ) { }

  ngOnInit(): void {
  }

  R0L(){
    this.Activo = 'R0L';
  }

  R1D(){
    this.Activo = 'R1D';
  }

  Lavadora(){
    this.Activo = 'Lavadora';
  }

  R0D(){
    this.Activo = 'R0D';
  }

  R1(){
    this.Activo = 'R1';
  }

  Prensa_1(){
    this.Activo = 'Prensa 1';
  }

  R2(){
    this.Activo = 'R2';
  }

  Prensa_2(){
    this.Activo = 'Prensa 2';
  }

  R3(){
    this.Activo = 'R3';
  }

  Prensa_3(){
    this.Activo = 'Prensa 3';
  }

  R4(){
    this.Activo = 'R4';
  }

  Prensa_4(){
    this.Activo = 'Prensa 4';
  }

  R5(){
    this.Activo = 'R5';
  }

  Prensa_5(){
    this.Activo = 'Prensa 5';
  }

  R7(){
    this.Activo = 'R7';
  }

  R8(){
    this.Activo = 'R8';
  }

  Apilado(){
    this.Activo = 'Apilado';
  }

  Banda(){
    this.Activo = 'Banda';
  }


}
