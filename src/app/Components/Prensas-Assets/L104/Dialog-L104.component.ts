import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-Dialog-L104',
  templateUrl: './Dialog-L104.component.html',
  styleUrls: ['./Dialog-L104.component.css']
})
export class DialogL104Component implements OnInit {

  Activo: string;

  Linea: string = 'Línea 104';

  constructor( public dialog: MatDialog ) { }

  ngOnInit(): void {
  }



  Desapilador(){
    this.Activo = 'Desapilador';
  }

  RH0(){
    this.Activo = 'RH0';
  }

  Lavadora(){
    this.Activo = 'Lavadora';
  }

  RH1(){
    this.Activo = 'RH1';
  }

  Prensa_1(){
    this.Activo = 'Prensa 1';
  }

  RH2(){
    this.Activo = 'RH2';
  }

  Prensa_2(){
    this.Activo = 'Prensa 2';
  }

  RH3(){
    this.Activo = 'RH3';
  }

  Prensa_3(){
    this.Activo = 'Prensa 3';
  }

  RH4(){
    this.Activo = 'RH4';
  }

  Prensa_4(){
    this.Activo = 'Prensa 4';
  }

  RH5(){
    this.Activo = 'RH5';
  }

  Banda1(){
    this.Activo = 'Banda 1';
  }

  Banda2(){
    this.Activo = 'Banda 2';
  }
}
