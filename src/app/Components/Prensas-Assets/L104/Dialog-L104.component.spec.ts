import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DialogL104Component } from './Dialog-L104.component';

describe('DialogL104Component', () => {
  let component: DialogL104Component;
  let fixture: ComponentFixture<DialogL104Component>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogL104Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogL104Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
