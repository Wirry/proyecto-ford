import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DialogL105Component } from './Dialog-L105.component';

describe('DialogL105Component', () => {
  let component: DialogL105Component;
  let fixture: ComponentFixture<DialogL105Component>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogL105Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogL105Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
