import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-Dialog-L105',
  templateUrl: './Dialog-L105.component.html',
  styleUrls: ['./Dialog-L105.component.css']
})
export class DialogL105Component implements OnInit {

  Activo: string;

  Linea: string = 'Línea 105';

  constructor( public dialog: MatDialog ) { }

  ngOnInit(): void {
  }
  Desapilador(){
    this.Activo = 'Desapilador';
  }

  R0L(){
    this.Activo = 'R0L';
  }

  R0D(){
    this.Activo = 'R0D';
  }

  R1(){
    this.Activo = 'R1';
  }

  Prensa_1(){
    this.Activo = 'Prensa 1';
  }

  R2(){
    this.Activo = 'R2';
  }

  Prensa_2(){
    this.Activo = 'Prensa 2';
  }

  R3(){
    this.Activo = 'R3';
  }

  Prensa_3(){
    this.Activo = 'Prensa 3';
  }

  R4(){
    this.Activo = 'R4';
  }

  Prensa_4(){
    this.Activo = 'Prensa 4';
  }

  R5(){
    this.Activo = 'R5';
  }

  Prensa_5(){
    this.Activo = 'Prensa 5';
  }

  R6(){
    this.Activo = 'R6';
  }

  Prensa_6(){
    this.Activo = 'Prensa 6';
  }

  Banda(){
    this.Activo = 'Banda';
  }


}
