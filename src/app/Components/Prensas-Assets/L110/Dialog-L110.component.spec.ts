import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DialogL110Component } from './Dialog-L110.component';

describe('DialogL110Component', () => {
  let component: DialogL110Component;
  let fixture: ComponentFixture<DialogL110Component>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogL110Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogL110Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
