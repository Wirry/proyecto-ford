import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-l110',
  templateUrl: './dialog-l110.component.html',
  styleUrls: ['./dialog-l110.component.css']
})
export class DialogL110Component implements OnInit {


  Activo: string;

  Linea: string = 'Línea 110';


  constructor( public dialog: MatDialog ) { }

  ngOnInit(): void {
  }

  R1(){
    this.Activo = 'R1';
  }

  Desapilador(){
    this.Activo = 'Desapilador';
  }

  R2(){
    this.Activo = 'R2';
  }

  Prensa(){
    this.Activo = 'Prensa';
  }

  Banda_1(){
    this.Activo = 'Banda 1';
  }

  Banda_2(){
    this.Activo = 'Banda 2';
  }

}
