import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { ValueConverter } from '@angular/compiler/src/render3/view/template';

@Component({
  selector: 'app-Dialog-L101',
  templateUrl: './Dialog-L101.component.html',
  styleUrls: ['./Dialog-L101.component.css']
})
export class DialogL101Component implements OnInit {

Activo: string;

Linea: string = 'Línea 101';

  constructor( public dialog: MatDialog ) { }

  ngOnInit(): void {
  }



  R0D(){
    this.Activo = 'R0D';
  }

  Lavadora(){
    this.Activo = 'Lavadora';
  }

  R0L(){
    this.Activo = 'R0L';
  }

  Prensa_1(){
    this.Activo = 'Prensa 1';
  }

  R1(){
    this.Activo = 'R1';
  }

  Prensa_2(){
    this.Activo = 'Prensa 2';
  }

  R2(){
    this.Activo = 'R2';
  }

  Prensa_3(){
    this.Activo = 'Prensa 3';
  }

  R3(){
    this.Activo = 'R3';
  }

  Prensa_4(){
    this.Activo = 'Prensa 4';
  }

  R4(){
    this.Activo = 'R4';
  }

  Prensa_5(){
    this.Activo = 'Prensa 5';
  }

  R5(){
    this.Activo = 'R5';
  }

  Banda(){
    this.Activo = 'Banda';
  }
}
