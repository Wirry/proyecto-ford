import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DialogL101Component } from './Dialog-L101.component';

describe('DialogL101Component', () => {
  let component: DialogL101Component;
  let fixture: ComponentFixture<DialogL101Component>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogL101Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogL101Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
