import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-l113',
  templateUrl: './dialog-l113.component.html',
  styleUrls: ['./dialog-l113.component.css']
})
export class DialogL113Component implements OnInit {

  Activo: string;

  Linea: string = 'Línea 113';

  constructor( public dialog: MatDialog ) { }

  ngOnInit(): void {
  }

  Desapilador(){
    this.Activo = 'Desapilador';
  }

  R0(){
    this.Activo = 'R0';
  }

  Lavadora(){
    this.Activo = 'Lavadora';
  }

  R1(){
    this.Activo = 'R1';
  }

  Prensa_1(){
    this.Activo = 'Prensa 1';
  }

  R2(){
    this.Activo = 'R2';
  }

  Prensa_2(){
    this.Activo = 'Prensa 2';
  }

  R3(){
    this.Activo = 'R3';
  }

  Prensa_3(){
    this.Activo = 'Prensa 3';
  }

  R4(){
    this.Activo = 'R4';
  }

  Prensa_4(){
    this.Activo = 'Prensa 4';
  }

  R5(){
    this.Activo = 'R5';
  }

  Prensa_5(){
    this.Activo = 'Prensa 5';
  }

  R6(){
    this.Activo = 'R6';
  }

  Banda1(){
    this.Activo = 'Banda 1';
  }

  Banda2(){
    this.Activo = 'Banda 2';
  }

}
