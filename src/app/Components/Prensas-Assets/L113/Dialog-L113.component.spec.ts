import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DialogL113Component } from './Dialog-L113.component';

describe('DialogL113Component', () => {
  let component: DialogL113Component;
  let fixture: ComponentFixture<DialogL113Component>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogL113Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogL113Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
