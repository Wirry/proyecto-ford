import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DialogU102Component } from './Dialog-U102.component';

describe('DialogU102Component', () => {
  let component: DialogU102Component;
  let fixture: ComponentFixture<DialogU102Component>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogU102Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogU102Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
