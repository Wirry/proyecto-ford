import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-u104',
  templateUrl: './dialog-u104.component.html',
  styleUrls: ['./dialog-u104.component.css']
})
export class DialogU104Component implements OnInit {

  Activo: string;

  Linea: string = 'Línea U104';

  constructor( public dialog: MatDialog ) { }

  ngOnInit(): void {
  }

  Devanadora(){
    this.Activo = 'Devanadora';
  }

  Enderezadora(){
    this.Activo = 'Enderezadora';
  }

  Prensa(){
    this.Activo = 'Prensa';
  }

  Apilador_1(){
    this.Activo = 'Apilador 1';
  }

  Apilador_2(){
    this.Activo = 'Apilador 2';
  }

  Cizalla(){
    this.Activo = 'Cizalla';
  }

}
