import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DialogU104Component } from './Dialog-U104.component';

describe('DialogU104Component', () => {
  let component: DialogU104Component;
  let fixture: ComponentFixture<DialogU104Component>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogU104Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogU104Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
