import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DialogL111Component } from './Dialog-L111.component';

describe('DialogL111Component', () => {
  let component: DialogL111Component;
  let fixture: ComponentFixture<DialogL111Component>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogL111Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogL111Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
