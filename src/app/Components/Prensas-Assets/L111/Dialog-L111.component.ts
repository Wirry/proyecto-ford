import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-l111',
  templateUrl: './dialog-l111.component.html',
  styleUrls: ['./dialog-l111.component.css']
})
export class DialogL111Component implements OnInit {

  Activo: string;

  Linea: string = 'Línea 111';

  constructor( public dialog: MatDialog ) { }

  ngOnInit(): void {
  }

  Desapilador(){
    this.Activo = 'Desapilador';
  }

  Lavadora(){
    this.Activo = 'Lavadora';
  }

  VC1(){
    this.Activo = 'Volvo Carga 1';
  }

  Prensa_1(){
    this.Activo = 'Prensa 1';
  }

  VD1(){
    this.Activo = 'Volvo Descarga 1';
  }

  Transportador_1(){
    this.Activo = 'Transportador 1';
  }

  VC2(){
    this.Activo = 'Volvo Carga 2';
  }

  Prensa_2(){
    this.Activo = 'Prensa 2';
  }
  VD2(){
    this.Activo = 'Volvo Descarga 2';
  }

  Transportador_2(){
    this.Activo = 'Transportador 2';
  }

  VC3(){
    this.Activo = 'Volvo Carga 3';
  }

  Prensa_3(){
    this.Activo = 'Prensa 3';
  }

  R4(){
    this.Activo = 'R4';
  }

  Prensa_4(){
    this.Activo = 'Prensa 4';
  }

  R5(){
    this.Activo = 'R5';
  }

  Prensa_5(){
    this.Activo = 'Prensa 5';
  }

  R1(){
    this.Activo = 'R1';
  }

  R2(){
    this.Activo = 'R2';
  }

  R3(){
    this.Activo = 'R3';
  }

  Apilado(){
    this.Activo = 'Apilado';
  }

  Banda1(){
    this.Activo = 'Banda 1';
  }

  Banda2(){
    this.Activo = 'Banda 2';
  }

}
