import { DialogLayoutGruasComponent } from './../DialogLayoutGruas/DialogLayoutGruas.component';
import { DialogLayoutMatriceriaComponent } from './../Dialog-LayoutMatriceria/Dialog-LayoutMatriceria.component';
import { DialogLayoutPrensasComponent } from './../Dialog-LayoutPrensas/Dialog-LayoutPrensas.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FormControl } from '@angular/forms'

@Component({
  selector: 'app-Insert',
  templateUrl: './Insert.component.html',
  styleUrls: ['./Insert.component.css']
})
export class InsertComponent implements OnInit {

  public Tiempo_Actual = new Date();
  time = {hour: this.Tiempo_Actual.getHours() , minute: this.Tiempo_Actual.getMinutes() };
  seleccion: string;
  TiposDeSeleccion: string[] = ['En cambio', 'En Try-Out', 'Programado', 'Por fugas', 'Sin Repuesto', 'Avería normal', 'Condición JMP', 'Falta de aceite', 'Pendiente', 'HPH',];
  TextoAveria: string;
  TextoCausa: string;
  TextoAccion: string;
  date = new FormControl(new Date());

  constructor(public dialog: MatDialog) {}

  openDialogEquipos() {
    this.dialog.open(DialogLayoutPrensasComponent);
  }

  openDialogLayoutMatriceria() {
    this.dialog.open(DialogLayoutMatriceriaComponent);
  }

  openDialogGruas() {
    this.dialog.open(DialogLayoutGruasComponent);
  }

  ngOnInit() {
  }

}
/* Tarjetas */
export class CardOverviewExample {}

