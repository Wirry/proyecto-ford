import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-Dialog-LayoutMatriceria',
  templateUrl: './Dialog-LayoutMatriceria.component.html',
  styleUrls: ['./Dialog-LayoutMatriceria.component.css']
})
export class DialogLayoutMatriceriaComponent implements OnInit {

  Activo: string;

  Linea: string = 'Matricería';

  constructor() { }

  ngOnInit() {
  }

  Prensa_SMG_1(){
    this.Activo = 'Prensa SMG 1';
  }

  Prensa_SMG_2(){
    this.Activo = 'Prensa SMG 2';
  }

  Prensa_Volante(){
    this.Activo = 'Prensa Volante';
  }

  Cizalla_Corte(){
    this.Activo = 'Cizalla Corte';
  }


}
