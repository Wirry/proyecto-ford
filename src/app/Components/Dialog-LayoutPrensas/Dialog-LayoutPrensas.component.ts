import { DialogL113Component } from './../Prensas-Assets/L113/Dialog-L113.component';
import { DialogU104Component } from '../Prensas-Assets/U104/Dialog-U104.component';
import { DialogU103Component } from '../Prensas-Assets/U103/Dialog-U103.component';
import { DialogU102Component } from '../Prensas-Assets/U102/Dialog-U102.component';
import { DialogU101Component } from '../Prensas-Assets/U101/Dialog-U101.component';
import { DialogL114Component } from '../Prensas-Assets/L114/Dialog-L114.component';
import { DialogL112Component } from '../Prensas-Assets/L112/Dialog-L112.component';
import { DialogL111Component } from '../Prensas-Assets/L111/Dialog-L111.component';
import { DialogL110Component } from '../Prensas-Assets/L110/Dialog-L110.component';
import { DialogL108Component } from '../Prensas-Assets/L108/Dialog-L108.component';
import { DialogL107Component } from '../Prensas-Assets/L107/Dialog-L107.component';
import { DialogL106Component } from '../Prensas-Assets/L106/Dialog-L106.component';
import { DialogL105Component } from '../Prensas-Assets/L105/Dialog-L105.component';
import { DialogL104Component } from '../Prensas-Assets/L104/Dialog-L104.component';
import { DialogL101Component } from '../Prensas-Assets/L101/Dialog-L101.component';


import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-Dialog-LayoutPrensas',
  templateUrl: './Dialog-LayoutPrensas.component.html',
  styleUrls: ['./Dialog-LayoutPrensas.component.css']
})
export class DialogLayoutPrensasComponent implements OnInit {

  constructor( public dialog: MatDialog ) { }



  openDialogL101() {
    this.dialog.open(DialogL101Component);
  }
  openDialogL104() {
    this.dialog.open(DialogL104Component);
  }
  openDialogL105() {
    this.dialog.open(DialogL105Component);
  }
  openDialogL106() {
    this.dialog.open(DialogL106Component);
  }
  openDialogL107() {
    this.dialog.open(DialogL107Component);
  }
  openDialogL108() {
    this.dialog.open(DialogL108Component);
  }
  openDialogL110() {
    this.dialog.open(DialogL110Component);
  }
  openDialogL111() {
    this.dialog.open(DialogL111Component);
  }
  openDialogL112() {
    this.dialog.open(DialogL112Component);
  }
  openDialogL113() {
    this.dialog.open(DialogL113Component);
  }
  openDialogL114() {
    this.dialog.open(DialogL114Component);
  }
  openDialogU101() {
    this.dialog.open(DialogU101Component);
  }
  openDialogU102() {
    this.dialog.open(DialogU102Component);
  }
  openDialogU103() {
    this.dialog.open(DialogU103Component);
  }
  openDialogU104() {
    this.dialog.open(DialogU104Component);
  }


  ngOnInit() {
  }

}

/* Tarjetas */
export class CardOverviewExample {}
