import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-navigate',
  templateUrl: './navigate.component.html',
  styleUrls: ['./navigate.component.css']
})
export class NavigateComponent {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

    title = 'Base de Datos de Mantenimiento de Prensas';

    isChecked = true;

    private daysArray = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Domingo'];
    private dia = new Date();
    
    public hora: any;
    public minuto: string;
    public segundo: string;
    public ampm: string;
    public dia_de_la_semana: string;

  constructor(private breakpointObserver: BreakpointObserver) {}

  ngOnInit() {
    setInterval(() => {
      const date = new Date();
      this.updateDate(date);
    }, 1000); // Esto actualiza el método cada segundo.

    this.dia_de_la_semana = this.daysArray[this.dia.getDay()];
    // getDay() devuelve el dia en formato entero, desde 0 a 6 toma el valor correspomdiente al día de daysArray.
    }

    private updateDate(date: Date) {
      const hours = date.getHours(); // coge las horas desde date
      this.hora = hours < 10 ? '0' + hours : hours.toString();   

      const minutes = date.getMinutes();
      this.minuto = minutes < 10 ? '0' + minutes : minutes.toString();

      const seconds = date.getSeconds();
      this.segundo = seconds < 10 ? '0' + seconds : seconds.toString();

    }


}
