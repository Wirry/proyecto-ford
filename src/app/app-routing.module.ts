import { InsertComponent } from './Components/Insert/Insert.component';
import { ReportsComponent } from './Components/Reports/Reports.component';
import { ModifyComponent } from './Components/Modify/Modify.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdministradorComponent } from './Components/Administrador/Administrador.component';


const routes: Routes = [
  { path: 'Insert', component: InsertComponent },
  { path: 'Modify', component: ModifyComponent },
  { path: 'Reports', component: ReportsComponent },
  { path: 'Administrador', component: AdministradorComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
