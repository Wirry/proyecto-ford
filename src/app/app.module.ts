import { DialogLayoutMatriceriaComponent } from './Components/Dialog-LayoutMatriceria/Dialog-LayoutMatriceria.component';
import { DialogLayoutPrensasComponent } from './Components/Dialog-LayoutPrensas/Dialog-LayoutPrensas.component';


import { ReportsComponent } from './Components/Reports/Reports.component';
import { ModifyComponent } from './Components/Modify/Modify.component';
import { InsertComponent } from './Components/Insert/Insert.component';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigateComponent } from './Components/navigate/navigate.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatMenuModule } from '@angular/material/menu';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/Input';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatRadioModule } from '@angular/material/radio';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCardModule } from '@angular/material/card';
import { MatTreeModule } from '@angular/material/tree';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { DialogL101Component } from './Components/Prensas-Assets/L101/Dialog-L101.component';
import { DialogL104Component } from './Components/Prensas-Assets/L104/Dialog-L104.component';
import { DialogL105Component } from './Components/Prensas-Assets/L105/Dialog-L105.component';
import { DialogL106Component } from './Components/Prensas-Assets/L106/Dialog-L106.component';
import { DialogL107Component } from './Components/Prensas-Assets/L107/Dialog-L107.component';
import { DialogL108Component } from './Components/Prensas-Assets/L108/Dialog-L108.component';
import { DialogL110Component } from './Components/Prensas-Assets/L110/Dialog-L110.component';
import { DialogL111Component } from './Components/Prensas-Assets/L111/Dialog-L111.component';
import { DialogL112Component } from './Components/Prensas-Assets/L112/Dialog-L112.component';
import { DialogL113Component } from './Components/Prensas-Assets/L113/Dialog-L113.component';
import { DialogL114Component } from './Components/Prensas-Assets/L114/Dialog-L114.component';
import { DialogU101Component } from './Components/Prensas-Assets/U101/Dialog-U101.component';
import { DialogU102Component } from './Components/Prensas-Assets/U102/Dialog-U102.component';
import { DialogU103Component } from './Components/Prensas-Assets/U103/Dialog-U103.component';
import { DialogU104Component } from './Components/Prensas-Assets/U104/Dialog-U104.component';
import { from } from 'rxjs';
import { AdministradorComponent } from './Components/Administrador/Administrador.component';






@NgModule({
  declarations: [
    AppComponent,
    NavigateComponent,
    ReportsComponent,
    ModifyComponent,
    InsertComponent,
    DialogL101Component,
    DialogL104Component,
    DialogL105Component,
    DialogL106Component,
    DialogL107Component,
    DialogL108Component,
    DialogL110Component,
    DialogL111Component,
    DialogL112Component,
    DialogL113Component,
    DialogL114Component,
    DialogU101Component,
    DialogU102Component,
    DialogU103Component,
    DialogU104Component,
    DialogLayoutMatriceriaComponent,
    DialogLayoutPrensasComponent,
    AdministradorComponent,       


  ],

  entryComponents: [DialogLayoutPrensasComponent, DialogLayoutMatriceriaComponent],

  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatMenuModule,
    MatMomentDateModule,
    MatDatepickerModule,
    NgbModule,
    FormsModule,
    NgbModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonToggleModule,
    MatRadioModule,
    MatDialogModule,
    MatCardModule,
    MatSlideToggleModule,
    ReactiveFormsModule,
    MatTreeModule,   
  ],
  
  exports: [
  ],

  providers: [
  {provide: MAT_DATE_LOCALE,  useValue: 'es-Es'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
